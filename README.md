## Module VAKSIN
**CD VAKSIN - Fitur 10 Dibuat oleh : Ramdhan - 2006595753**

## URLs

| namespace         | URL 				                | Fitur                 |
| ----------------- | --------------------------------- | --------------------- |
| .....             | ..... 			                | .....                 |
| create_vaksin     | /create_vaksin                    | CD VAKSIN - Fitur 10  |
| delete_vaksin     | /delete_vaksin/(?P<vaksin>[^/]+)$ | CD VAKSIN - Fitur 10  |

## Module users (Fitur Wajib)
**Dibuat oleh : Ramdhan - 2006595753**

Digunakan untuk login/registrasi/logout

Untuk mengecek apakah user sudah login maka bisa panggil perintah berikut,

```
request.session.get("pengguna", None)   # Return identifikasi user
request.session.get("tipe", None)       # Return "Warga", "Admin Satgas", atau "Panitia Penyelenggara"
```

Jika sudah logged in, akan dikembalikan identifikasi user. Jika belum logged in, akan dikembalikan value `None`

## URLs

| namespace         | URL 				| 
| ----------------- | ------------------| 
| login             | /login 			|
| logout            | /logout 			|
| register          | /logout 			|
| register_warga    | /register_warga 	|
| register_admin    | /register_admin	|
| register_panitia  | /register_panitia |

## Module CR Tiket
**CR Tiket - Fitur 10 Dibuat oleh : Ramdhan - 2006595753**

## URLs

| namespace     | URL 				                                                                                | Fitur                 |
| ------------- | ------------------------------------------------------------------------------------------------- | --------------------- |
| tiket_saya    | /tiket                                                                                            | CR Tiket - Fitur 9    |
| detail_kartu  | /tiket/detail/(?P<instansi>[0-9]+)$/(?P<jadwal>[0-9]+)$/(?P<nomor>[0-9]+)$/(?P<status>[0-9]+)$    | CR Tiket - Fitur 9    |
| jadwal        | /tiket/jadwal_vaksinasi                                                                           | CR Tiket - Fitur 9    |
| daftar        | /tiket/daftar_vaksinasi/(?P<instansi>[0-9]+)$/(?P<jadwal>[0-9]+)$                                 | CR Tiket - Fitur 9    |
| mendaftar     | /tiket/daftar_vaksinasi/mendaftar/(?P<instansi>[0-9]+)$/(?P<jadwal>[0-9]+)$                       | CR Tiket - Fitur 9    |
