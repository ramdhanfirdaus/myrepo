from django.forms import ModelForm, TextInput
from .models import OrderItem

class NoteForm(ModelForm):
    class Meta:
        model = OrderItem
        fields = ['note']
        widgets = {
            'note' : TextInput(attrs={'class': 'form-control', 'style':'flex:8', 'placeholder':'Tuliskan catatan pemesanan disini'}),
        }